FROM node:latest
RUN mkdir /app
RUN cd ./client && yarn build && mv build ../

ADD ./ /app