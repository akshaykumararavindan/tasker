package routes

import (
	"context"
	"io"
	"log"
	s "main/sockets"
	"net/http"

	"github.com/gorilla/mux"
)

// type CORSOptions struct {
// 	Header  handlers.CORSOption
// 	Origins handlers.CORSOption
// 	Methods handlers.CORSOption
// }

func init() {

}

func serveReact(w http.ResponseWriter, r *http.Request) {
	log.Print(r.Method)
	var ctx context.Context
	log.Print(ctx)
	io.WriteString(w, "Hello, world!\n")
	w.Write([]byte("hello"))
}

func ServeStatic(r *mux.Router) {
	// Serve static files
	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("./build/static/"))))

	// Serve index page on all unhandled routes
	r.PathPrefix("/").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "../build/index.html")
	})
}

func MountRoutes(r *mux.Router) {
	r.Use(mux.CORSMethodMiddleware(r))
	r.HandleFunc("/data", serveReact).Methods(http.MethodGet, http.MethodPost)
	r.HandleFunc("/ws", s.SocketHandler).Methods(http.MethodGet, http.MethodPost)
	// r.Handle("socket", s.Server)
}
