package routes

import (
	"log"
	"net/http"
	"os"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

type CORSOptions struct {
	Header  handlers.CORSOption
	Origins handlers.CORSOption
	Methods handlers.CORSOption
}

var c CORSOptions

func StartServer() {
	r := mux.NewRouter()
	MountRoutes(r)
	ServeStatic(r)
	PORT, _ := os.LookupEnv("PORT")
	c.Header = handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization"})
	c.Methods = handlers.AllowedMethods([]string{"GET", "POST", "PUT", "HEAD", "OPTIONS"})
	c.Origins = handlers.AllowedOrigins([]string{"*"})

	err := http.ListenAndServe(":5000", handlers.CORS(c.Header, c.Methods, c.Origins)(r))
	if err != nil {
		// if err := http.ListenAndServe(":5000", r); err != nil {
		log.Fatal("Error starting server", err)
	}
	log.Printf("Server running on %s", PORT)

}
