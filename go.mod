module main

go 1.16

require (
	github.com/googollee/go-socket.io v1.4.4
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/websocket v1.4.2
	github.com/lib/pq v1.10.1
)
