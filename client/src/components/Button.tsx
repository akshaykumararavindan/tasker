import React, { useEffect, useState } from "react";

const Button: React.FC = () => {
  const [count, setCount] = useState<number>(0);
  const getData = async () => {
    try {
      const result = await fetch(`http://localhost:5000/data`);
      console.log(result);
    } catch (error) {}
  };
  useEffect(() => {
    getData();
  }, [count]);
  return (
    <div>
      <p>You clicked the button {count} times</p>
      <button onClick={() => setCount(count + 1)}>Update count</button>
    </div>
  );
};
export { Button };
