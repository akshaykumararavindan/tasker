import React, {useEffect, useState} from 'react'
import {sendMessage} from "../store/actions"

const MessageInput: React.FC = () => {
    const [message, setMessage] = useState<string>("")
    const [formStyle, setFormStyle] = useState<{}>()
    console.log(message)
    useEffect(() => {
        const style= {display: "flex", flexDirection: "row", backgroundColor: "red", alignItems: "center"}
        setFormStyle(style)
        return () => {
            
        }
    }, [])
    const sendMessageFunction = async () => {
       let result =  await sendMessage(message)
       console.log(result)
    }
    return(
        <div style={formStyle}>
            <form method="post">
                <div>
                    <label htmlFor="messages">Messages</label>
                    <input onChange={e => setMessage(e.target.value)} value={message} name="messages" type="text"/>
                </div>
                <div>
                    <button onClick={sendMessageFunction} type="button" value="send">Send</button>
                </div>
            </form>
        </div>
    )
}

export default MessageInput