import axios from "axios"
import {io} from 'socket.io-client'
interface data {
    from?: string
    to?: string
    roomId?: number
    body: string
}


export const sendMessage: any = async(data: data) => {
    const url = "http://localhost:5000/ws"
    const socket = io("http://localhost:5000/ws")
    console.log(socket.id)
    socket.on("connect", () => {
        console.log(socket.id)
    })
    let body = JSON.stringify(data)
    // let token = localStorage.getItem('')
    // let config = {
    //     Authorization: `Bearer ${token}`,
    //     Headers: {
    //         'Content-Type': 'application/json'
    //     }
    // } 
    let result: any = await axios.post("http://localhost:5000/data", body)
    return result
}

