import React from "react";
import "./App.css";
import { Button } from "./components/Button";
import MessagePage from "./components/MessagePage"

function App() {
  return (
    <div className="App">
      <Button />
      <MessagePage />
    </div>
  );
}

export default App;
