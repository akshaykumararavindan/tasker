package main

import (
	"log"
	"main/db"
	"main/routes"
	"os"
)

func main() {
	db.DBConn()
	val, ok := os.LookupEnv("PORT")
	if ok {
		log.Print("14", val)
	}
	routes.StartServer()
}
